# test_addition.py
from src.calculator import add
import pytest

def test_add ():
    result = add (3, 7)
    assert result == 10

def test_add_string ():
    with pytest.raises (TypeError):
        add ("string", 4)
