# calculator.py
def checkInputs (a, b):
    if not isinstance (a, (int, float)) or not isinstance (b, (int, float)):
        raise TypeError ("les inputs doivent être de type int ou float!")

def add (a, b):
    checkInputs (a, b)
    return a + b

